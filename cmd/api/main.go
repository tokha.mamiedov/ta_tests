package main

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"net/http"
	"ta_tests/internal/app/v1/delivery"
	"ta_tests/internal/app/v1/delivery/httpdelivery"
	"ta_tests/internal/app/v1/repository/postgres"
	"ta_tests/internal/app/v1/service"
	"ta_tests/pkg/config"
)

var (
	enableHttps = false
)

func main() {
	addr := flag.String("addr", "127.0.0.1:80", "addr")
	cfg := flag.String("c", "cfg/config.yml", "config file path")
	flag.Parse()
	err := config.NewConfig(*cfg)
	if err != nil {
		log.Fatal(err)
	}

	log.SetLevel(log.DebugLevel)
	db, err := postgres.NewPostgresDB(config.Cfg.PgConf)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rep, err := postgres.NewPostgresRepository(db)
	if err != nil {
		log.Fatal(err)
	}
	serv, err := service.NewService(rep)
	if err != nil {
		log.Fatal(err)
	}
	d, err := delivery.NewDelivery(serv)
	if err != nil {
		log.Fatal(err)
	}

	router := d.InitRoutes()

	c := d.HTTP.CorsMiddleware(httpdelivery.Origin)
	server := http.Server{
		Addr:    *addr,
		Handler: c.Handler(router),
	}

	err = server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}

}
