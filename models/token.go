package models

import (
	"github.com/twinj/uuid"
)

var (
	TeacherRole = "teacher"
	StudentRole = "student"
)

type AccessToken struct {
	ProfileWithGroup
	Exp int64 `json:"exp,omitempty"`
}

type RefreshToken struct {
	UUID uuid.UUID `db:"uuid"`
	Exp  int64     `db:"expires"`
}

type RefreshTokenMeta struct {
	RefreshToken
	ProfileID uint64 `db:"profile_id"`
	UserAgent string `db:"user_agent"`
	Created   string `db:"created"`
}

type TokenDetails struct {
	AccessToken        string
	AccessTokenPayload AccessToken
	RefreshTokenMeta   RefreshTokenMeta
}

type CreateTokenData struct {
	ProfileWithGroup
	UserAgent string
}
