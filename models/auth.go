package models

type Authorization struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type AuthorizationWithUserAgent struct {
	Authorization
	UserAgent string
}

type Registration struct {
	ProfileWithGroup
	TeacherKey string `json:"teacher_key"`
	Password   string `json:"password"`
}

type AuthData struct {
	RefreshToken RefreshToken
	AccessToken  string      `json:"access_token"`
	Profile      interface{} `json:"profile"`
}
