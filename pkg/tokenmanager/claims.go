package tokenmanager

import (
	"ta_tests/models"
	apperror "ta_tests/pkg/error"
	"time"
)

type Claims struct {
	models.AccessToken
	UUID string `json:"uuid,omitempty"`
}

func (c Claims) Valid() error {
	now := time.Now().Unix()
	if ok := c.verifyExpiresAt(now, false); !ok {
		return apperror.TokenExpired
	}

	return nil
}

func (c Claims) verifyExpiresAt(now int64, req bool) bool {
	if c.Exp == 0 {
		return !req
	}

	return now <= c.Exp
}
