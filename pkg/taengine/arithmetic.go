package taengine

import (
	"errors"
	"strconv"
)

var (
	TooBigNumberError  = errors.New("too big number")
	ItIsNotNumberError = errors.New("it is not number")
)

//func DirectToReverse(i string) (string, error) {
//	if len(i) > 8 {
//		return "", TooBigNumberError
//	}
//
//	negative := false
//	if i[0] == '1' {
//		i = "-" + i[1:]
//		negative = true
//	}
//
//	if !negative {
//		return i, nil
//	}
//
//	num, err := strconv.ParseInt(i, 2, 8)
//	if err != nil {
//		return "", ItIsNotNumberError
//	}
//	b := &bitset.BitSet{}
//	b = b.Set(8)
//	num = -num
//	b = b.Flip(uint(num))
//
//	return "", nil
//}

func AddDirectCode(i1 string, i2 string) (string, error) {
	if len(i1) > 8 || len(i2) > 8 {
		return "", TooBigNumberError
	}

	if i1[0] == '1' {
		i1 = "-" + i1[1:]
	}

	if i2[0] == '1' {
		i2 = "-" + i2[1:]
	}

	num1, err := strconv.ParseInt(i1, 2, 8)
	if err != nil {
		return "", ItIsNotNumberError
	}

	num2, err := strconv.ParseInt(i2, 2, 8)
	if err != nil {
		return "", ItIsNotNumberError
	}

	res := strconv.FormatInt(num1+num2, 2)
	negative := false
	if res[0] == '-' {
		res = "1" + res[1:]
		negative = true
	}

	return addPrefix(res, negative, '0'), nil
}

func addPrefix(n string, negative bool, pSym rune) string {
	if len(n) >= 8 {
		return n
	}

	prefix := []rune{}

	for i := 0; i < 8-len(n); i++ {
		prefix = append(prefix, pSym)
	}
	if negative {
		return n[:1] + string(prefix) + n[1:]
	}

	return string(prefix) + n
}
