package taengine

import (
	"testing"
)

func TestAddDirectCode(t *testing.T) {
	i1 := "00001000"  // 8
	i2 := "00001000"  // 8
	res := "00010000" //16
	n, _ := AddDirectCode(i1, i2)
	if n != res {
		t.Errorf("expected %s, got %s", res, n)
	}
}

func TestAddDirectCodeWithNegativeOperand(t *testing.T) {
	i1 := "10001000"  // -8
	i2 := "00000100"  // 4
	res := "10000100" // -4
	n, _ := AddDirectCode(i1, i2)
	if n != res {
		t.Errorf("expected %s, got %s", res, n)
	}
}

func TestAddDirectCodeWithAllNegativeOperand(t *testing.T) {
	i1 := "10001000"  // -8
	i2 := "10000100"  // -4
	res := "10001100" // -12
	n, _ := AddDirectCode(i1, i2)
	if n != res {
		t.Errorf("expected %s, got %s", res, n)
	}
}

//func TestDirectToReverseNegative(t *testing.T) {
//	i := "10001000"   // -8
//	res := "11110111" // reverse code -8
//
//	n, _ := DirectToReverse(i)
//	if n != res {
//		t.Errorf("expected %s, got %s", res, n)
//	}
//}
