package httpdelivery

import (
	"net/http"
	"ta_tests/models"
	"time"
)

func (d Delivery) setRefreshTokenCookie(token models.RefreshToken, w http.ResponseWriter) {
	cookies := []http.Cookie{
		{
			Name:     "refresh_token",
			Value:    token.UUID.String(),
			Expires:  time.Unix(token.Exp, 0),
			HttpOnly: true,
		},
	}

	for _, cookie := range cookies {
		http.SetCookie(w, &cookie)
	}
}
