package repository

import (
	"context"
	"github.com/twinj/uuid"
	"ta_tests/models"
)

type Profile interface {
	CreateProfile(ctx context.Context, profile models.Profile, pass string) (models.Profile, error)
	GetProfileWithPassByLogin(ctx context.Context, login string) (models.ProfileWithPassword, error)
	GetProfileById(ctx context.Context, ID uint64) (models.ProfileWithGroup, error)
}

type Group interface {
	CreateGroup(ctx context.Context, name string) (models.Group, error)
	CreateProfileGroupRelation(ctx context.Context, profileID uint64, groupID uint64) error
	SelectGroupByProfileId(ctx context.Context, profileID uint64) (models.Group, error)
}

type RefreshToken interface {
	CreateRefreshToken(ctx context.Context, token models.RefreshTokenMeta) (models.RefreshTokenMeta, error)
	SelectRefreshTokenByProfileIdUserAgent(ctx context.Context, profileID uint64, userAgent string) (models.RefreshTokenMeta, error)
	DeleteRefreshTokenByProfileIdUserAgent(ctx context.Context, profileID uint64, userAgent string) error
	SelectRefreshTokenByUuidUserAgent(ctx context.Context, UUID uuid.UUID, userAgent string) (models.RefreshTokenMeta, error)
	DeleteRefreshTokenByUuidUserAgent(ctx context.Context, UUID uuid.UUID, userAgent string) error
}

type Repository interface {
	Profile
	RefreshToken
	Group
	Begin() (RepositoryTx, error)
}

type RepositoryTx interface {
	Repository
	Commit() error
	Rollback() error
}
