package postgres

import (
	"context"
	"errors"
	"github.com/jmoiron/sqlx"
	"github.com/twinj/uuid"
	"ta_tests/models"
	"ta_tests/pkg/logger"
)

const (
	insertRefreshToken = `INSERT INTO ta_tests.refresh_token (profile_id, uuid, user_agent, expires) 
                          VALUES (:profile_id, :uuid, :user_agent, :expires) 
                          RETURNING profile_id, uuid, user_agent, expires, created`
	selectRefreshTokenByProfileIdUserAgent = `SELECT * FROM ta_tests.refresh_token WHERE profile_id=$1 AND user_agent=$2`
	selectRefreshTokenByUuidUserAgent      = `SELECT * FROM ta_tests.refresh_token WHERE uuid=$1 AND user_agent=$2`
	deleteRefreshTokenByProfileIdUserAgent = `DELETE FROM ta_tests.refresh_token WHERE profile_id=$1 AND user_agent=$2`
	deleteRefreshTokenByUuidUserAgent      = `DELETE FROM ta_tests.refresh_token WHERE uuid=$1 AND user_agent=$2`
)

type RefreshToken struct {
	db sqlx.Ext
}

func NewRefreshToken(db sqlx.Ext) (*RefreshToken, error) {
	if db == nil {
		return nil, errors.New("expected db, got nil")
	}

	return &RefreshToken{db: db}, nil
}

func (rt RefreshToken) CreateRefreshToken(ctx context.Context, token models.RefreshTokenMeta) (models.RefreshTokenMeta, error) {
	rows, err := sqlx.NamedQuery(rt.db, insertRefreshToken, token)
	if err != nil {
		logger.LoggingError(ctx, logger.RepositoryLocation, err)
		return models.RefreshTokenMeta{}, err
	}
	refreshTokenMeta := models.RefreshTokenMeta{}
	for rows.Next() {
		err = rows.StructScan(&refreshTokenMeta)
		if err != nil {
			logger.LoggingError(ctx, logger.RepositoryLocation, err)
			return models.RefreshTokenMeta{}, err
		}
	}

	logger.LoggingDebug(ctx, logger.RepositoryLocation, map[string]interface{}{
		"Refresh token meta inserted in postgres": refreshTokenMeta,
	})

	return refreshTokenMeta, err
}

func (rt RefreshToken) SelectRefreshTokenByProfileIdUserAgent(ctx context.Context, profileID uint64, userAgent string) (models.RefreshTokenMeta, error) {
	rtMeta := models.RefreshTokenMeta{}
	err := sqlx.Get(rt.db, &rtMeta, selectRefreshTokenByProfileIdUserAgent, profileID, userAgent)
	if err != nil {
		logger.LoggingError(ctx, logger.RepositoryLocation, err)
		return models.RefreshTokenMeta{}, err
	}

	return rtMeta, err
}

func (rt RefreshToken) SelectRefreshTokenByUuidUserAgent(ctx context.Context, UUID uuid.UUID, userAgent string) (models.RefreshTokenMeta, error) {
	rtMeta := models.RefreshTokenMeta{}
	err := sqlx.Get(rt.db, &rtMeta, selectRefreshTokenByUuidUserAgent, UUID, userAgent)
	if err != nil {
		logger.LoggingError(ctx, logger.RepositoryLocation, err)
		return models.RefreshTokenMeta{}, err
	}

	return rtMeta, err
}

func (rt RefreshToken) DeleteRefreshTokenByProfileIdUserAgent(ctx context.Context, profileID uint64, userAgent string) error {
	_, err := rt.db.Queryx(deleteRefreshTokenByProfileIdUserAgent, profileID, userAgent)
	if err != nil {
		logger.LoggingError(ctx, logger.RepositoryLocation, err)
	}

	return err
}

func (rt RefreshToken) DeleteRefreshTokenByUuidUserAgent(ctx context.Context, UUID uuid.UUID, userAgent string) error {
	_, err := rt.db.Queryx(deleteRefreshTokenByUuidUserAgent, UUID, userAgent)
	if err != nil {
		logger.LoggingError(ctx, logger.RepositoryLocation, err)
	}

	return err
}
