package service

import (
	"context"
	"crypto/sha1"
	"errors"
	"fmt"
	"github.com/twinj/uuid"
	"ta_tests/internal/app/v1/repository"
	"ta_tests/models"
	"ta_tests/pkg/config"
	apperror "ta_tests/pkg/error"
	"ta_tests/pkg/logger"
	"ta_tests/pkg/tokenmanager"
	"time"
)

type AuthService struct {
	rep          repository.Repository
	tokenManager tokenmanager.TokenManager
}

func NewAuthService(rep repository.Repository) (*AuthService, error) {
	if rep == nil {
		return nil, errors.New("expected repository, got nil")
	}

	tm, err := tokenmanager.NewManager(config.Cfg.SigningAccessKey)
	if err != nil {
		return nil, err
	}
	return &AuthService{
		rep:          rep,
		tokenManager: tm,
	}, nil
}

func (a AuthService) SignUpTeacher(ctx context.Context, reg models.Registration, userAgent string) (models.AuthData, error) {
	tx, err := a.rep.Begin()
	if err != nil {
		return models.AuthData{}, err
	}
	defer processingEndTx(ctx, tx, &err)

	a.rep = tx

	// Проверяем корректен ли преподавательский ключ
	if reg.TeacherKey != config.Cfg.TeacherKey {
		return models.AuthData{}, errors.New("bad teacher key")
	}

	// Общая регистрация
	auth, err := a.signUp(ctx, reg, userAgent)

	return auth, err
}

func (a AuthService) SignUpStudent(ctx context.Context, reg models.Registration, userAgent string) (models.AuthData, error) {
	tx, err := a.rep.Begin()
	if err != nil {
		return models.AuthData{}, err
	}
	defer processingEndTx(ctx, tx, &err)
	a.rep = tx

	// Пробуем создать группу
	g, err := a.rep.CreateGroup(ctx, reg.Group)
	err = apperror.NewServiceError(err)

	// Если дупликат, значит группв уже создана
	if err != nil && err != apperror.DuplicateError {
		return models.AuthData{}, apperror.NewServiceError(err)
	}
	if err == apperror.DuplicateError {
		logger.LoggingDebug(ctx, logger.ServiceLocation, fmt.Sprintf("Group %s already exists", g.Name))
	}

	// Общая регистрация
	aData, err := a.signUp(ctx, reg, userAgent)

	if err != nil {
		return models.AuthData{}, err
	}

	// Создаем связть профиля с грруппой
	p := aData.Profile.(models.Profile)
	err = a.rep.CreateProfileGroupRelation(ctx, p.ID, g.ID)
	if err != nil {
		return models.AuthData{}, apperror.NewServiceError(err)
	}

	aData.Profile = models.ProfileWithGroup{
		Profile: p,
		Group:   g.Name,
	}

	return aData, nil
}

func (a AuthService) signUp(ctx context.Context, reg models.Registration, userAgent string) (models.AuthData, error) {
	tx, err := a.rep.Begin()
	if err != nil {
		return models.AuthData{}, err
	}
	defer processingEndTx(ctx, tx, &err)
	a.rep = tx

	// Проверяем корректность роли (учитель/ученик)
	if reg.Role != models.TeacherRole && reg.Role != models.StudentRole {
		return models.AuthData{}, apperror.BadRoleError
	}

	// Записываем профиль в базу
	p, err := a.rep.CreateProfile(ctx, reg.Profile, generatePasswordHash(reg.Password))
	if err != nil {
		return models.AuthData{}, apperror.NewServiceError(err)
	}

	// Создаем токены
	tokenDetails, err := a.createTokens(ctx, models.CreateTokenData{
		ProfileWithGroup: models.ProfileWithGroup{Profile: p},
		UserAgent:        userAgent,
	})
	if err != nil {
		logger.LoggingError(ctx, logger.ServiceLocation, err)
		return models.AuthData{}, apperror.NewServiceError(err)
	}

	// Записываем рефреш токен в базу
	_, err = a.rep.CreateRefreshToken(ctx, tokenDetails.RefreshTokenMeta)
	if err != nil {
		logger.LoggingError(ctx, logger.ServiceLocation, errors.New("refresh token not created"))
		return models.AuthData{}, apperror.NewServiceError(err)
	}

	return models.AuthData{
		Profile:      p,
		RefreshToken: tokenDetails.RefreshTokenMeta.RefreshToken,
		AccessToken:  tokenDetails.AccessToken,
	}, nil
}

func (a AuthService) SignIn(ctx context.Context, auth models.AuthorizationWithUserAgent) (models.AuthData, error) {
	tx, err := a.rep.Begin()
	if err != nil {
		return models.AuthData{}, err
	}
	defer processingEndTx(ctx, tx, &err)
	a.rep = tx
	// Достаем профиль с паролем из базы
	p, err := a.rep.GetProfileWithPassByLogin(ctx, auth.Login)
	if err != nil {
		return models.AuthData{}, apperror.NewServiceError(err)
	}

	// Проверяем существует ли профиль с таким логином
	if p == (models.ProfileWithPassword{}) {
		logger.LoggingDebug(ctx, logger.ServiceLocation, fmt.Sprintf("profile with login %s not found", auth.Login))
		return models.AuthData{}, apperror.RecordNotFound
	}

	// Проверяем правильность пароля
	if passEqual(auth.Password, p.Pass) == false {
		return models.AuthData{}, apperror.BadPasswordError
	}

	// Достаем группу из базы если студент
	group := models.Group{}
	if p.Role == models.StudentRole {
		group, err = a.rep.SelectGroupByProfileId(ctx, p.ID)
		if err != nil {
			return models.AuthData{}, err
		}
	}

	// Пробуем достать из базы рефреш токен с присланными profile_id и user_agent
	rt, err := a.rep.SelectRefreshTokenByProfileIdUserAgent(ctx, p.ID, auth.UserAgent)
	if err != nil {
		return models.AuthData{}, apperror.NewServiceError(err)
	}

	// Проверяем что в базе нет рефреш токена с тем же profile_id и user_agent
	// Если есть, удаляем
	if rt != (models.RefreshTokenMeta{}) {
		logger.LoggingDebug(ctx, logger.ServiceLocation, map[string]interface{}{
			fmt.Sprintf("refresh token with profile_id=%u, user_agent=%s already exists", p.ID, auth.UserAgent): rt,
		})

		err = a.rep.DeleteRefreshTokenByProfileIdUserAgent(ctx, p.ID, auth.UserAgent)

		if err != nil {
			return models.AuthData{}, apperror.NewServiceError(err)
		}
	}

	// Создаем токены
	tokenDetails, err := a.createTokens(ctx, models.CreateTokenData{
		ProfileWithGroup: models.ProfileWithGroup{
			Profile: p.Profile,
			Group:   group.Name,
		},
		UserAgent: auth.UserAgent,
	})
	if err != nil {
		logger.LoggingError(ctx, logger.ServiceLocation, err)
	}

	// Записываем рефреш токен в базу
	_, err = a.rep.CreateRefreshToken(ctx, tokenDetails.RefreshTokenMeta)

	if err != nil {
		logger.LoggingError(ctx, logger.ServiceLocation, errors.New("refresh token not created"))
		return models.AuthData{}, apperror.NewServiceError(err)
	}

	authData := models.AuthData{
		Profile:      p.Profile,
		RefreshToken: tokenDetails.RefreshTokenMeta.RefreshToken,
		AccessToken:  tokenDetails.AccessToken,
	}

	if p.Role == models.StudentRole {
		authData.Profile = models.ProfileWithGroup{
			Profile: p.Profile,
			Group:   group.Name,
		}
	}

	return authData, nil

}

func (a AuthService) Identity(ctx context.Context, accessToken string) (models.AccessToken, error) {
	at, err := a.tokenManager.ParseAccessToken(accessToken)
	if err != nil && err != apperror.TokenExpired {
		logger.LoggingError(ctx, logger.ServiceLocation, err)
		return models.AccessToken{}, apperror.NewServiceError(err)
	}

	if err == apperror.TokenExpired {
		return models.AccessToken{}, err
	}

	return at, nil
}

func (a AuthService) Refresh(ctx context.Context, UUID uuid.UUID, userAgent string) (string, models.RefreshToken, error) {
	tx, err := a.rep.Begin()
	if err != nil {
		return "", models.RefreshToken{}, err
	}
	defer processingEndTx(ctx, tx, &err)
	a.rep = tx
	rtMeta, err := a.rep.SelectRefreshTokenByUuidUserAgent(ctx, UUID, userAgent)
	if err != nil {
		return "", models.RefreshToken{}, apperror.NewServiceError(err)
	}

	if rtMeta == (models.RefreshTokenMeta{}) {
		return "", models.RefreshToken{}, apperror.InvalidRefreshTokenError
	}

	if time.Now().Unix() > rtMeta.Exp {
		return "", models.RefreshToken{}, apperror.InvalidRefreshTokenError
	}

	err = a.rep.DeleteRefreshTokenByUuidUserAgent(ctx, UUID, userAgent)
	if err != nil {
		return "", models.RefreshToken{}, apperror.NewServiceError(err)
	}

	p, err := a.rep.GetProfileById(ctx, rtMeta.ProfileID)
	if err != nil {
		return "", models.RefreshToken{}, apperror.NewServiceError(err)
	}

	tokenDetails, err := a.createTokens(ctx, models.CreateTokenData{
		ProfileWithGroup: p,
		UserAgent:        userAgent,
	})

	_, err = a.rep.CreateRefreshToken(ctx, tokenDetails.RefreshTokenMeta)
	if err != nil {
		return "", models.RefreshToken{}, apperror.NewServiceError(err)
	}

	return tokenDetails.AccessToken, tokenDetails.RefreshTokenMeta.RefreshToken, nil
}

func (a AuthService) createTokens(ctx context.Context, p models.CreateTokenData) (models.TokenDetails, error) {
	accessToken, accessTokenPayload, err := a.tokenManager.NewAccessJWT(models.AccessToken{
		ProfileWithGroup: p.ProfileWithGroup},
		config.Cfg.AccessTokenTTL)
	if err != nil {
		return models.TokenDetails{}, err
	}

	if err != nil {
		return models.TokenDetails{}, err
	}

	refreshTokenMeta := models.RefreshTokenMeta{
		ProfileID: p.ID,
		UserAgent: p.UserAgent,
		RefreshToken: models.RefreshToken{
			UUID: uuid.NewV4(),
			Exp:  time.Now().Add(config.Cfg.RefreshTokenTTL).Unix(),
		},
	}

	logger.LoggingDebug(ctx, logger.ServiceLocation, map[string]interface{}{
		"created new access token":   accessTokenPayload,
		"created refresh token meta": refreshTokenMeta,
	})

	return models.TokenDetails{
		AccessToken:        accessToken,
		AccessTokenPayload: accessTokenPayload,
		RefreshTokenMeta:   refreshTokenMeta,
	}, nil
}

func generatePasswordHash(password string) string {
	hash := sha1.New()
	hash.Write([]byte(password))

	return fmt.Sprintf("%x", hash.Sum([]byte(config.Cfg.Salt)))
}

func passEqual(pass string, dbPass string) bool {
	pass = generatePasswordHash(pass)
	return pass == dbPass
}
