package service

import (
	"context"
	"errors"
	"github.com/twinj/uuid"
	"ta_tests/internal/app/v1/repository"
	"ta_tests/models"
	"ta_tests/pkg/logger"
)

type Auth interface {
	SignUpTeacher(ctx context.Context, reg models.Registration, userAgent string) (models.AuthData, error)
	SignUpStudent(ctx context.Context, reg models.Registration, userAgent string) (models.AuthData, error)
	SignIn(ctx context.Context, auth models.AuthorizationWithUserAgent) (models.AuthData, error)
	Identity(ctx context.Context, accessToken string) (models.AccessToken, error)
	Refresh(ctx context.Context, UUID uuid.UUID, userAgent string) (string, models.RefreshToken, error)
}

type Service struct {
	Auth
}

func NewService(r repository.Repository) (*Service, error) {
	if r == nil {
		return nil, errors.New("expected repository, got nil")
	}
	a, err := NewAuthService(r)
	if err != nil {
		return nil, err
	}

	return &Service{Auth: a}, nil
}

func processingEndTx(ctx context.Context, tx repository.RepositoryTx, err *error) {
	var txErr error
	if *err != nil {
		txErr = tx.Rollback()
		if txErr != nil {
			logger.LoggingError(ctx, logger.ServiceLocation, txErr)
		}
		return
	}

	txErr = tx.Commit()
	if txErr != nil {
		logger.LoggingError(ctx, logger.ServiceLocation, txErr)
	}
}
