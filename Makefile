ADDR="127.0.0.1:80"
CFG=""
.PHONY: build
build:
	go build -o bin/ta_tests_app -v ./cmd/api

.PHONY: run
run:
	./bin/ta_tests_app -addr=$(ADDR) -c=$(CFG) &> stdout.log

.PHONY: stop

stop:
	killall -9 ta_tests_app

.PHONY: db
db:
	psql -U ta_tests -d ta_tests -a -f schema.sql

.DEFAULT_GOAL := build