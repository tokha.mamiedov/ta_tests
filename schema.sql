DROP SCHEMA IF EXISTS ta_tests CASCADE;
CREATE
    EXTENSION IF NOT EXISTS CITEXT;
CREATE SCHEMA ta_tests;

CREATE TABLE ta_tests.tests
(
    id       SERIAL PRIMARY KEY NOT NULL,
    test_id  INT UNIQUE         NOT NULL,
    semester INT                NOT NULL
);

CREATE TABLE ta_tests.profile
(
    "id"       SERIAL       NOT NULL,
    "login"    VARCHAR(255) NOT NULL,
    "name"     CITEXT       NOT NULL,
    "surname"  CITEXT       NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "role"     VARCHAR(8)   NOT NULL,
    PRIMARY KEY ("id"),
    UNIQUE (login)
);

CREATE TABLE ta_tests."group"
(
    "id"   SERIAL NOT NULL,
    "name" CITEXT NOT NULL,
    PRIMARY KEY ("id"),
    UNIQUE ("name")
);

CREATE TABLE ta_tests.profile_group
(
    "profile_id" INT NOT NULL,
    "group_id"   INT NOT NULL,
    PRIMARY KEY ("profile_id", "group_id"),
    FOREIGN KEY ("profile_id") REFERENCES ta_tests.profile ("id") ON DELETE CASCADE,
    FOREIGN KEY ("group_id") REFERENCES ta_tests."group" ("id") ON DELETE CASCADE

);

CREATE TABLE ta_tests.refresh_token
(
    "uuid"       UUID PRIMARY KEY         NOT NULL,
    "profile_id" INT REFERENCES ta_tests.profile (id) ON DELETE CASCADE,
    "user_agent" CITEXT                   NOT NULL,
    "expires"    BIGINT                   NOT NULL,
    "created"    timestamp with time zone NOT NULL DEFAULT now(),
    UNIQUE ("profile_id", "user_agent")
);