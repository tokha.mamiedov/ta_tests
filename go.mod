module ta_tests

go 1.13

require (
	github.com/bits-and-blooms/bitset v1.2.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/csrf v1.7.1
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.4
	github.com/rs/cors v1.8.2
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.10.1 // indirect
	github.com/twinj/uuid v1.0.0
	gopkg.in/yaml.v2 v2.4.0
)
